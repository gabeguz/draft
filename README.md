## draft

Draft aims to make writing easy. It's a command line program that
creates files for you, allows you to edit those files with your editor, and
helps you organize and find those files later.

## Install

Install: 

go get gitlab.com/gabeguz/draft

## Usage

```
Usage:
  draft [command]

Available Commands:
  create      Create a new draft
  help        Help about any command
  list        List drafts of [type]

Flags:
  -h, --help   help for draft

Use "draft [command] --help" for more information about a command.
```
