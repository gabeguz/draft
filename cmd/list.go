// Copyright 2015 Gabriel Guzman <gabe@lifewaza.com>
// All rights reserved.
// Use of this source code is governed by the ISC
// license that can be found in the LICENSE file.

package cmd

import (
	"context"
	"fmt"
	"os"
	"path/filepath"
	"sort"
	"strings"
	"time"

	"github.com/njones/particle"
	"github.com/spf13/afero"
	"github.com/urfave/cli/v3"
)

// Draft holds metadata about the draft
type Draft struct {
	kind    string
	title   string
	path    string
	modTime time.Time
}

// List existing drafts, and chose one to continue editing
// walk directory tree from drafts root folder
// display with meta-data: Title, Date

func List(ctx context.Context, draft *cli.Command) error {
	root, err := filepath.EvalSymlinks(os.Getenv("HOME") + "/drafts")
	if err != nil {
		fmt.Println(err)
	}
	draftkinds := []string{}
	drafts := []Draft{}
	if len(draft.Args().Get(0)) > 0 {
		draftkinds = append(draftkinds, draft.Args().Get(0))
	}
	err = filepath.Walk(root, func(path string, info os.FileInfo, err error) error {
		var kind string
		// Skip hidden directories
		if info.IsDir() && strings.HasPrefix(info.Name(), ".") {
			return filepath.SkipDir
		}
		if info.IsDir() {
			kind = info.Name()
		} else {
			var fs = afero.NewOsFs()
			b, err := afero.ReadFile(fs, path)
			if err != nil {
				fmt.Println(err)
			}

			metadata := struct {
				Title string
			}{}

			_, err = particle.TOMLEncoding.DecodeString(string(b), &metadata)
			if err != nil {
				fmt.Printf("TOML decoding error in file %s: %v\n", info.Name(), err)
			}

			drafts = append(drafts, Draft{kind, metadata.Title, path, info.ModTime()})
		}
		return nil
	})
	if err != nil {
		fmt.Println(err)
	}

	// sort files by ModTime
	sort.Slice(drafts, func(i, j int) bool {
		return drafts[i].modTime.Before(drafts[j].modTime)
	})

	fmt.Printf("Kinds: %v\n", draftkinds)
	for _, draft := range drafts {
		if len(draftkinds) != 0 {
			for i := 0; i < len(draftkinds); i++ {
				if draftkinds[i] == draft.kind {
					fmt.Printf("%s\t%s\t%s\n", draft.path, draft.kind, draft.title)
				}
			}
		} else {
			// display all drafts
			fmt.Printf("%s\t%s\t%s\n", draft.path, draft.kind, draft.title)
		}

	}
	return nil
}
