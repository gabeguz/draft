package cmd

import (
	"context"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"

	"github.com/gosimple/slug"
	"github.com/njones/particle"
	"github.com/spf13/afero"
	"github.com/urfave/cli/v3"
)

// Rename temp files with what is in the title TOML if any

func Reconcile(ctx context.Context, draft *cli.Command) error {
	// determine default draft directory
	root, err := filepath.EvalSymlinks(os.Getenv("HOME") + "/drafts")
	if err != nil {
		log.Fatal(err)
	}

	err = filepath.Walk(root, func(path string, info os.FileInfo, err error) error {
		// Skip hidden directories
		if info.IsDir() && strings.HasPrefix(info.Name(), ".") {
			return filepath.SkipDir
		}
		// Skip directories
		if info.IsDir() {
			return filepath.SkipDir
		}

		var fs = afero.NewOsFs()
		b, err := afero.ReadFile(fs, path)
		if err != nil {
			fmt.Println(err)
		}

		metadata := struct {
			Title string
		}{}

		_, err = particle.TOMLEncoding.DecodeString(string(b), &metadata)
		if err != nil {
			fmt.Printf("TOML decoding error in file %s: %v\n", info.Name(), err)
		}

		if len(metadata.Title) > 0 {
			if draft.Bool("slug") {
				if info.Name() != slug.Make(metadata.Title)+".md" {
					if _, err := os.Stat(filepath.Join(root, slug.Make(metadata.Title)+".md")); err == nil {
						fmt.Printf("File %s exists do nothing.\n", slug.Make(metadata.Title)+".md")
					} else {
						fmt.Printf("Rename %s -> %s\n", info.Name(), slug.Make(metadata.Title)+".md")
						os.Rename(filepath.Join(root, info.Name()), filepath.Join(root, slug.Make(metadata.Title)+".md"))
					}
				} else {
					fmt.Printf("%s already same as %s, do nothing.\n", info.Name(), slug.Make(metadata.Title)+".md")
				}
			} else {
				if info.Name() != metadata.Title+".md" {
					if _, err := os.Stat(filepath.Join(root, slug.Make(metadata.Title)+".md")); err == nil {
						fmt.Printf("File %s exists do nothing.\n", metadata.Title+".md")
					} else {
						fmt.Printf("Rename %s -> %s\n", info.Name(), metadata.Title+".md")
						os.Rename(filepath.Join(root, info.Name()), filepath.Join(root, metadata.Title+".md"))
					}
				} else {
					fmt.Printf("%s already same as %s, do nothing.\n", info.Name(), metadata.Title+".md")
				}
			}
		}
		fmt.Printf("No title, do nothing.\n")
		return nil
	})
	if err != nil {
		fmt.Println(err)
	}
	return nil
}
