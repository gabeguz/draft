// Copyright 2015 Gabriel Guzman <gabe@lifewaza.com>
// All rights reserved.
// Use of this source code is governed by the ISC
// license that can be found in the LICENSE file.

package cmd

import (
	"context"
	"fmt"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"strings"

	"github.com/gosimple/slug"
	"github.com/urfave/cli/v3"
)

// Draft is for writing drafts.
const defaultEditor = "vi"

const frontMatter = `+++
title = ""
date = ""
tags = [""]
+++`

// Config holds user specified configuration information
type Config struct {
	User struct {
		Name  string
		Email string
	}
	Post struct {
		Root string
	}
	Twitter struct {
		ConsumerKey       string
		ConsumerSecret    string
		AccessToken       string
		AccessTokenSecret string
	}
	File struct {
		Root string
	}
}

// getEditor determines the editor the user would like to use to edit drafts
// in order of preference: GIT_EDITOR, VISUAL, EDITOR, or defaultEditor (vi)
func getEditor() string {
	editor, ok := os.LookupEnv("GIT_EDITOR")
	if !ok {
		editor, ok = os.LookupEnv("VISUAL")
		if !ok {
			editor, ok = os.LookupEnv("EDITOR")
			if !ok {
				editor = defaultEditor
			}
		}
	}
	return editor
}

// getRootDir determines the root directory for drafts.
// If a directory is passed on the command line with the -d flag, it will use that,
// otherwise it will use the default (~/notes/).
// If a type is passed on the command line with the -t flag, it will append the type
// to the root directory, for example "~/notes/blog/".
func getRootDir(dir, typeName string) (string, error) {
	var rootDir string

	if dir != "" {
		rootDir = dir
	} else {
		homeDir, err := os.UserHomeDir()
		if err != nil {
			return "", fmt.Errorf("failed to get user home directory: %w", err)
		}
		rootDir = filepath.Join(homeDir, "notes")
	}

	if typeName != "" {
		rootDir = filepath.Join(rootDir, typeName)
	}

	// Ensure the directory exists
	if err := os.MkdirAll(rootDir, 0755); err != nil {
		return "", fmt.Errorf("failed to create directory: %w", err)
	}

	return rootDir, nil
}

func Create(ctx context.Context, draft *cli.Command) error {

	rootDir, err := getRootDir(draft.String("dir"), draft.String("type"))
	if err != nil {
		log.Fatal(err)
	}

	editor := getEditor()

	// invoke editor
	var f *os.File
	fname := draft.Args().Get(0)
	if len(fname) > 0 {
		if draft.Bool("slug") {
			f, err = os.OpenFile(filepath.Join(rootDir, slug.Make(fname)+".md"), os.O_RDWR|os.O_CREATE, 0644)
		} else {
			f, err = os.OpenFile(filepath.Join(rootDir, fname+".md"), os.O_RDWR|os.O_CREATE, 0644)
		}
		if err != nil {
			log.Fatal(err)
		}
	} else {
		f, err = os.CreateTemp(rootDir, "*.md")
		if err != nil {
			log.Fatal("error in runCreate ", err)
		}
	}

	fname = strings.TrimSpace(f.Name())

	fmt.Printf("File: %s\n", f.Name())
	fmt.Printf("File: %v\n", f)

	// write frontmatter to file
	if _, err := f.WriteString(frontMatter); err != nil {
		log.Fatal("error writing to file ", err)
	}

	err = f.Close()
	if err != nil {
		log.Fatal("error closing file ", err)
	}
	command := exec.Command(editor, fname)
	command.Stdin = os.Stdin
	command.Stdout = os.Stdout
	command.Stderr = os.Stderr
	err = command.Run()
	if err != nil {
		log.Fatal("error trying to run editor ", err)
	}
	return nil
}
