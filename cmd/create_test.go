package cmd

import (
	"os"
	"path/filepath"
	"testing"
)

func TestGetEditor(t *testing.T) {
	// Save original environment variables
	originalGitEditor := os.Getenv("GIT_EDITOR")
	originalVisual := os.Getenv("VISUAL")
	originalEditor := os.Getenv("EDITOR")

	// Clean up environment variables after the test
	defer func() {
		os.Setenv("GIT_EDITOR", originalGitEditor)
		os.Setenv("VISUAL", originalVisual)
		os.Setenv("EDITOR", originalEditor)
	}()

	tests := []struct {
		name     string
		envSetup func()
		want     string
	}{
		{
			name: "GIT_EDITOR set",
			envSetup: func() {
				os.Setenv("GIT_EDITOR", "git-editor")
				os.Unsetenv("VISUAL")
				os.Unsetenv("EDITOR")
			},
			want: "git-editor",
		},
		{
			name: "VISUAL set, GIT_EDITOR not set",
			envSetup: func() {
				os.Unsetenv("GIT_EDITOR")
				os.Setenv("VISUAL", "visual-editor")
				os.Unsetenv("EDITOR")
			},
			want: "visual-editor",
		},
		{
			name: "EDITOR set, GIT_EDITOR and VISUAL not set",
			envSetup: func() {
				os.Unsetenv("GIT_EDITOR")
				os.Unsetenv("VISUAL")
				os.Setenv("EDITOR", "editor")
			},
			want: "editor",
		},
		{
			name: "No environment variables set",
			envSetup: func() {
				os.Unsetenv("GIT_EDITOR")
				os.Unsetenv("VISUAL")
				os.Unsetenv("EDITOR")
			},
			want: defaultEditor,
		},
		{
			name: "All environment variables set, GIT_EDITOR takes priority",
			envSetup: func() {
				os.Setenv("GIT_EDITOR", "git-editor")
				os.Setenv("VISUAL", "visual-editor")
				os.Setenv("EDITOR", "editor")
			},
			want: "git-editor",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.envSetup()
			if got := getEditor(); got != tt.want {
				t.Errorf("getEditor() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestGetRootDir(t *testing.T) {
	// Save the original home directory and restore it after the test
	originalHome := os.Getenv("HOME")
	defer os.Setenv("HOME", originalHome)

	// Set a fake home directory for testing
	fakeHome := "/fake/home"
	os.Setenv("HOME", fakeHome)

	tests := []struct {
		name     string
		dir      string
		typeName string
		want     string
		wantErr  bool
	}{
		{
			name:     "Default directory",
			dir:      "",
			typeName: "",
			want:     filepath.Join(fakeHome, "notes"),
			wantErr:  false,
		},
		{
			name:     "Custom directory",
			dir:      "/custom/dir",
			typeName: "",
			want:     "/custom/dir",
			wantErr:  false,
		},
		{
			name:     "Default directory with type",
			dir:      "",
			typeName: "blog",
			want:     filepath.Join(fakeHome, "notes", "blog"),
			wantErr:  false,
		},
		{
			name:     "Custom directory with type",
			dir:      "/custom/dir",
			typeName: "journal",
			want:     filepath.Join("/custom/dir", "journal"),
			wantErr:  false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := getRootDir(tt.dir, tt.typeName)
			if (err != nil) != tt.wantErr {
				t.Errorf("getRootDir() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("getRootDir() = %v, want %v", got, tt.want)
			}

			// Check if the directory was created
			if _, err := os.Stat(got); os.IsNotExist(err) {
				t.Errorf("getRootDir() did not create the directory %s", got)
			}

			// Clean up: remove the created directory
			os.RemoveAll(got)
		})
	}
}
