// Copyright 2014 Gabriel Guzman <gabe@lifewaza.com>
// All rights reserved.
// Use of this source code is governed by the ISC
// license that can be found in the LICENSE file.

package twitter

import (
	"fmt"
	"log"

	"github.com/ChimeraCoder/anaconda"
	"gitlab.com/gabeguz/draft/poster"
)

const maxPostLength = 140

// Config holds local configuration
type Config struct {
	ConsumerKey       string
	ConsumerSecret    string
	AccessToken       string
	AccessTokenSecret string
}

var config Config

// Poster is the concrete implementation of the poster interface
type Poster struct {
}

// Post posts a message
func (t *Poster) Post(message poster.Message) (err error) {
	if len(message.Body) > maxPostLength {
		log.Printf("Message too long for twitter, not posting. Message is %v chars long, max is %v\n",
			len(message.Body), maxPostLength)
		return
	}

	fmt.Println("Posting to twitter...")
	anaconda.SetConsumerKey(config.ConsumerKey)
	anaconda.SetConsumerSecret(config.ConsumerSecret)
	api := anaconda.NewTwitterApi(config.AccessToken, config.AccessTokenSecret)
	postResult, err := api.PostTweet(message.Body, nil)
	if err != nil {
		log.Panic(err)
	}

	fmt.Printf("success: %s\n", postResult.CreatedAt)

	return err
}

// SetConfig sets the local configuration
func SetConfig(twitterConfig Config) {
	config = twitterConfig
}
