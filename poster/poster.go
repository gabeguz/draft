// Package poster provides interfaces for various posters
package poster

// Message defines the message being posted as well as it's metadata
type Message struct {
	Title    string
	Body     string
	Author   string
	FileName string
}

// Poster can take a Message and post it somewhere
type Poster interface {

	// Post takes a Message, and posts it somewhere
	Post(Message) error
}
