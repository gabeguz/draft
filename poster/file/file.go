// Copyright 2014 Gabriel Guzman <gabe@lifewaza.com>
// All rights reserved.
// Use of this source code is governed by the ISC
// license that can be found in the LICENSE file.

package file

import (
	"fmt"
	"hash/fnv"
	"io"
	"io/ioutil"
	"log"
	"os"
	"strconv"
	"time"

	"gitlab.com/gabeguz/draft/poster"
)

// Config holds user specificed configuration data
type Config struct {
	Root string
}

type filePaths struct {
	Root   string
	Drafts string
	Posts  string
}

// Poster is the concrete type for the poster interface
type Poster struct {
}

var config Config
var paths filePaths

func prepareDirs() (err error) {
	// Set default paths
	paths.Root = config.Root
	paths.Drafts = config.Root + "/drafts/"
	paths.Posts = config.Root + "/posts/"
	// Make sure paths exist
	err = os.MkdirAll(paths.Root, 0750)
	if err != nil {
		log.Fatal(err)
	}
	err = os.MkdirAll(paths.Drafts, 0750)
	if err != nil {
		log.Fatal(err)
	}
	err = os.MkdirAll(paths.Posts, 0750)
	if err != nil {
		log.Fatal(err)
	}
	return err
}

// Post posts a message
func (f *Poster) Post(message poster.Message) (err error) {
	err = prepareDirs()
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("Post will be saved in dir: %s\n", paths.Drafts)
	// Save the post to a file
	year, month, day := time.Now().Date()
	syear := strconv.FormatInt(int64(year), 10)
	sday := strconv.FormatInt(int64(day), 10)
	fmt.Printf("Year: %s Month: %s Day %s\n", syear, month, sday)
	if err != nil {
		log.Fatal(err)
	}
	// copy post to drafts directory
	err = os.MkdirAll(config.Root+"/drafts/", 0750)
	srcfile, err := os.Open(message.FileName)
	if err != nil {
		log.Fatal(err)
	}
	defer srcfile.Close()
	contents, err := ioutil.ReadFile(message.FileName)
	if err != nil {
		log.Fatal(err)
	}
	h := fnv.New32a()
	if _, err := io.WriteString(h, string(contents)); err != nil {
		log.Fatal(err)
	}
	hash := h.Sum(nil)
	hashString := fmt.Sprintf("%x", hash)
	dstfile, err := os.Create(paths.Drafts + hashString)
	if err != nil {
		log.Panic(err)
	}
	defer dstfile.Close()
	if _, err := io.Copy(dstfile, srcfile); err != nil {
		log.Panic(err)
	}
	os.Remove(message.FileName)
	return err
}

// SetConfig sets the local config from the passed in global config
// TODO: I don't think I use this
func SetConfig(fileConfig Config) {
	config = fileConfig
}
