// Copyright 2014 Gabriel Guzman <gabe@lifewaza.com>
// All rights reserved.
// Use of this source code is governed by the ISC
// license that can be found in the LICENSE file.

package echo

import (
	"fmt"

	"gitlab.com/gabeguz/draft/poster"
)

// Poster is the concrete type for the poster interface
type Poster struct {
}

// Post posts a message
func (e *Poster) Post(message poster.Message) (err error) {
	fmt.Println("Your post is: ", message.Body)
	fmt.Println("Your post is: ", len(message.Body), " chars long.")
	return err
}
