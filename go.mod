module gitlab.com/gabeguz/draft

go 1.22.3

require (
	github.com/ChimeraCoder/anaconda v2.0.0+incompatible
	github.com/gosimple/slug v1.12.0
	github.com/njones/particle v0.0.0-20161212183752-f447b6333c0f
	github.com/spf13/afero v1.2.2
	github.com/urfave/cli/v3 v3.0.0-beta1
)

require (
	github.com/BurntSushi/toml v1.4.0 // indirect
	github.com/ChimeraCoder/tokenbucket v0.0.0-20131201223612-c5a927568de7 // indirect
	github.com/azr/backoff v0.0.0-20160115115103-53511d3c7330 // indirect
	github.com/dustin/go-jsonpointer v0.0.0-20160814072949-ba0abeacc3dc // indirect
	github.com/dustin/gojson v0.0.0-20160307161227-2e71ec9dd5ad // indirect
	github.com/garyburd/go-oauth v0.0.0-20180319155456-bca2e7f09a17 // indirect
	github.com/gosimple/unidecode v1.0.1 // indirect
	github.com/kr/pretty v0.1.0 // indirect
	golang.org/x/net v0.0.0-20200222125558-5a598a2470a0 // indirect
	golang.org/x/text v0.3.7 // indirect
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
