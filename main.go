package main

import (
	"context"
	"log"
	"os"

	"github.com/urfave/cli/v3"
	"gitlab.com/gabeguz/draft/cmd"
)

func main() {
	draft := &cli.Command{
		Name:  "draft",
		Usage: "draft [global options] command [command options] [arguments...]",
		Authors: []any{
			"Gabriel Guzman <gabe@lifewaza.com>",
		},
		Copyright: "(c) 2013 Gabriel Guzman",

		Commands: []*cli.Command{
			{
				Name:    "create",
				Aliases: []string{"c"},
				Usage:   "create a draft",
				Action:  cmd.Create,
				Flags: []cli.Flag{
					&cli.StringFlag{
						Name:    "type",
						Aliases: []string{"t"},
						Usage:   "type of draft",
					},
					&cli.StringFlag{
						Name:    "dir",
						Aliases: []string{"d"},

						Usage: "directory to create draft in",
					},
					&cli.BoolFlag{
						Name:    "slug",
						Aliases: []string{"s"},
						Usage:   "slugify the title",
					},
				},
			},
			{
				Name:    "list",
				Aliases: []string{"ls"},
				Usage:   "list drafts",
				Action:  cmd.List,
			},
			{
				Name:    "reconcile",
				Aliases: []string{"r"},
				Usage:   "rename all files to match their titles from frontmatter metadata",
				Action:  cmd.Reconcile,
			},
		},
	}

	if err := draft.Run(context.Background(), os.Args); err != nil {
		log.Fatal(err)
	}
}
